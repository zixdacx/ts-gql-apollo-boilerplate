import React from "react";
import styled from "styled-components";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
const index = () => {
  const JOBS_QUERY = gql`
    query {
      jobs {
        id
        title
        applyUrl
        company {
          name
        }
      }
    }
  `;
  const query = useQuery(JOBS_QUERY);
  interface IJob {
    id: string;
    applyUrl: string;
    title: string;
    company: {
      name: string;
    };
  }
  interface IJobs {
    jobs: IJob[];
  }

  return (
    <Wrapper>
      <JobList>
        {query?.data?.jobs.map((item: IJob, index) => (
          <Job key={index}>{item.title}</Job>
        ))}
      </JobList>
    </Wrapper>
  );
};

export default index;

const Wrapper = styled.div``;
const JobList = styled.div``;
const Job = styled.div``;
