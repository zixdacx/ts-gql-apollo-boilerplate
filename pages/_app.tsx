import React from "react";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { ApolloProvider } from "@apollo/react-hooks";
import withApollo from "../src/hooks/withApollo";
import { ApolloClient, NormalizedCacheObject } from "apollo-boost";
import GlobalStyle from "../src/styles/global";
import myTheme from "../src/styles/myTheme";

interface IProps {
  apollo: ApolloClient<NormalizedCacheObject>;
}

const MyApp = ({ Component, pageProps, apollo }) => {
  return (
    <>
      <Head>
        <title>Zix&Dacx</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <ApolloProvider client={apollo}>
        <ThemeProvider theme={myTheme}>
          <GlobalStyle />
          <Component {...pageProps} />
        </ThemeProvider>
      </ApolloProvider>
    </>
  );
};

export default withApollo(MyApp);
