import baseStyled, {
  DefaultTheme,
  css,
  ThemedStyledInterface,
} from "styled-components";

const customMediaQuery = (maxWidth: number) => `
      @media (max-width:${maxWidth}px)
    `;
const medias = {
  mobile: customMediaQuery(1040),
  desktop: customMediaQuery(4520),
};

const myTheme: DefaultTheme = {
  colors: {},
  medias,
};

export type Theme = typeof myTheme;
export const styled = baseStyled as ThemedStyledInterface<Theme>;
export default myTheme;
