import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    #__next {
        position: relative;
    }
  html,
  body,
  div,
  span,
  applet,
  object,
  iframe,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  blockquote,
  pre,
  /* a, */
  abbr,
  acronym,
  address,
  big,
  cite,
  code,
  del,
  dfn,
  em,
  font,
  img,
  ins,
  kbd,
  q,
  s,
  samp,
  small,
  strike,
  strong,
  sub,
  sup,
  tt,
  var,
  dl,
  dt,
  dd,
  ol,
  ul,
  li,
  fieldset,
  form,
  label,
  legend,
  table,
  caption,
  tbody,
  tfoot,
  thead,
  tr,
  th,
  td {
    margin: 0;
    padding: 0;
  }
  body,
  input,
  textarea,
  select,
  button,
  table {
    font-size: 16px;
    line-height: 1.25em;
  }
  html,
  body {
    padding: 0;
    margin: 0;
    scroll-behavior: smooth;
  }
  img,
  fieldset {
    border: 0;
  }
  ul,
  ol {
    list-style: none;
  }
  em,
  address {
    font-style: normal;
  }
  a {
    text-decoration: none;
    color: inherit;
  }
  a:hover,
  a:active,
  a:focus {
    cursor: pointer;
  }
  a:visited {
  }
  button,
  input[type="submit"],
  input[type="reset"] {
    background: none;
    color: inherit;
    padding: 0;
    font: inherit;
    cursor: pointer;
    outline: inherit;
  }
  input[type="checkbox"] {
    -webkit-appearance: checkbox !important;
    -moz-appearance: checkbox !important;
    -ms-appearance: checkbox !important;
    -o-appearance: checkbox !important;
    appearance: checkbox !important;
  }
  select {
    -webkit-appearance: none; /*네이티브 외형 감추기*/
    -moz-appearance: none;
    /* appearance: none; */
    border-radius: 0;
    text-align-last: center;
  }
  /* IE 10, 11의 네이티브 화살표 숨기기 */
  select::-ms-expand {
    display: none;
  }
  input {
    
  }
  textarea{
    color: #E0E0E9;
    line-height: 1.6em;
  }
  textarea:focus,
  input:focus,
  select:focus{
    outline: none;
  }
  
  
  button:focus,
  div:focus {
    outline:none;
  }
  :-ms-input-placeholder {
  }
  ::placeholder {
  }
  `;

export default GlobalStyle;
